#pragma once

#include "Loophole.hpp"

#include <cstddef>

// Don't redefine UniqueTag template argument
template<typename T = size_t, T StartValue = 0, auto UniqueTag = []() {}>
class ReflectionCounter
{
  using UniqueType = ReflectionCounter<T, StartValue, UniqueTag>;

public:
  constexpr ReflectionCounter()
  {
    loophole::checkUniqueness<UniqueType>();
  }

  ReflectionCounter(const ReflectionCounter& other) = delete;
  ReflectionCounter& operator=(const ReflectionCounter& other) = delete;

  ReflectionCounter(ReflectionCounter&& other) = default;
  ReflectionCounter& operator=(ReflectionCounter&& other) = default;

  // Don't redefine template arguments
  template<T CurrentValue = StartValue, auto OptimisationBlocker = []() {}>
  static consteval T getValue()
  {
    if constexpr (checkLoophole<CurrentValue>()) {
      return getValue<CurrentValue + 1>();
    } else {
      return CurrentValue;
    }
  }

  // Don't redefine template arguments
  template<auto OptimisationBlocker = []() {}>
  static consteval T increment()
  {
    return add<1>() - 1;
  }

  // Don't redefine template arguments CurrentValue and OptimisationBlocker
  template<size_t Addition, T CurrentValue = StartValue, auto OptimisationBlocker = []() {}>
  static consteval T add()
  {
    if constexpr (Addition == 0) {
      return getValue();
    } else {
      if constexpr (checkLoophole<CurrentValue>()) {
        return add<Addition, CurrentValue + 1>();
      } else {
        setLoophole<CurrentValue>();
        return add<Addition - 1, CurrentValue + 1>();
      }
    }
  }

private:
  template<T Value, auto OptimisationBlocker = []() {}>
  static consteval bool checkLoophole()
  {
    return loophole::checkLoophole<loophole::ReflectionTag<UniqueType, loophole::ValueTag<Value>>>();
  }

  template<T Value, auto OptimisationBlocker = []() {}>
  static consteval void setLoophole()
  {
    loophole::setLoophole<loophole::ReflectionLoophole<UniqueType, loophole::ValueTag<Value>>>();
  }
};
