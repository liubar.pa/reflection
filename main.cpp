#include <cassert>
#include "ReflectionCounter.hpp"
#include "TypeIndexer.hpp"

#include <string>
#include <iostream>

void validateReflectCounter();
void validateTypeIndexer();

int main()
{
  validateReflectCounter();
  validateTypeIndexer();
}


void validateReflectCounter()
{
  auto t = ReflectionCounter();
  assert(t.getValue() == 0);

  t.increment();
  assert(t.getValue() == 1);

  t.increment();
  assert(t.getValue() == 2);

  t.increment();
  t.increment();
  assert(t.getValue() == 4);

  t.add<0>();
  assert(t.getValue() == 4);

  t.add<1>();
  assert(t.getValue() == 5);

  t.add<2>();
  assert(t.getValue() == 7);

  t.add<5>();
  assert(t.getValue() == 12);

  auto i = std::move(t);
  assert(i.getValue() == 12);

  i.increment();
  assert(i.getValue() == 13);

  i.add<5>();
  assert(t.getValue() == 18);

  auto m = ReflectionCounter();
  assert(m.getValue() == 0);

  m.increment();
  assert(m.getValue() == 1);

  m.increment();
  assert(m.getValue() == 2);

  m.increment();
  m.increment();
  assert(m.getValue() == 4);

  m.add<0>();
  assert(m.getValue() == 4);

  m.add<1>();
  assert(m.getValue() == 5);

  m.add<2>();
  assert(m.getValue() == 7);

  m.add<5>();
  assert(m.getValue() == 12);

  auto n = std::move(m);
  assert(n.getValue() == 12);

  n.increment();
  assert(n.getValue() == 13);

  n.add<5>();
  assert(n.getValue() == 18);
}

void validateTypeIndexer()
{
  auto t = TypeIndexer();

  assert(t.addType<int>() == 0);
  assert(t.addType<float>() == 1);
  assert(t.addType<double>() == 2);
  assert(t.addType<std::string>() == 3);
  assert(t.addType<char>() == 4);

  assert(*t.getIndex<std::string>() == 3);
  assert(*t.getIndex<float>() == 1);
  assert(*t.getIndex<int>() == 0);
  assert(*t.getIndex<double>() == 2);
  assert(*t.getIndex<char>() == 4);
  assert(t.getIndex<long>() == std::nullopt);
  assert(t.getIndex<size_t>() == std::nullopt);

  assert(t.isIndexed<std::string>() == true);
  assert(t.isIndexed<float>() == true);
  assert(t.isIndexed<int>() == true);
  assert(t.isIndexed<double>() == true);
  assert(t.isIndexed<char>() == true);
  assert(t.isIndexed<long>() == false);
  assert(t.isIndexed<size_t>() == false);

  auto i = std::move(t);

  assert(i.addType<int>() == 0);
  assert(i.addType<float>() == 1);
  assert(i.addType<double>() == 2);
  assert(i.addType<std::string>() == 3);
  assert(i.addType<char>() == 4);

  assert(*i.getIndex<std::string>() == 3);
  assert(*i.getIndex<float>() == 1);
  assert(*i.getIndex<int>() == 0);
  assert(*i.getIndex<double>() == 2);
  assert(*i.getIndex<char>() == 4);
  assert(i.getIndex<long>() == std::nullopt);
  assert(i.getIndex<size_t>() == std::nullopt);

  assert(i.isIndexed<std::string>() == true);
  assert(i.isIndexed<float>() == true);
  assert(i.isIndexed<int>() == true);
  assert(i.isIndexed<double>() == true);
  assert(i.isIndexed<char>() == true);
  assert(i.isIndexed<long>() == false);
  assert(i.isIndexed<size_t>() == false);

  auto a = TypeIndexer();

  assert(a.addType<int>() == 0);
  assert(a.addType<float>() == 1);
  assert(a.addType<double>() == 2);
  assert(a.addType<std::string>() == 3);
  assert(a.addType<char>() == 4);

  assert(*a.getIndex<std::string>() == 3);
  assert(*a.getIndex<float>() == 1);
  assert(*a.getIndex<int>() == 0);
  assert(*a.getIndex<double>() == 2);
  assert(*a.getIndex<char>() == 4);
  assert(a.getIndex<long>() == std::nullopt);
  assert(a.getIndex<size_t>() == std::nullopt);

  assert(a.isIndexed<std::string>() == true);
  assert(a.isIndexed<float>() == true);
  assert(a.isIndexed<int>() == true);
  assert(a.isIndexed<double>() == true);
  assert(a.isIndexed<char>() == true);
  assert(a.isIndexed<long>() == false);
  assert(a.isIndexed<size_t>() == false);

  auto b = std::move(t);

  assert(b.addType<int>() == 0);
  assert(b.addType<float>() == 1);
  assert(b.addType<double>() == 2);
  assert(b.addType<std::string>() == 3);
  assert(b.addType<char>() == 4);

  assert(*b.getIndex<std::string>() == 3);
  assert(*b.getIndex<float>() == 1);
  assert(*b.getIndex<int>() == 0);
  assert(*b.getIndex<double>() == 2);
  assert(*b.getIndex<char>() == 4);
  assert(b.getIndex<long>() == std::nullopt);
  assert(b.getIndex<size_t>() == std::nullopt);

  assert(b.isIndexed<std::string>() == true);
  assert(b.isIndexed<float>() == true);
  assert(b.isIndexed<int>() == true);
  assert(b.isIndexed<double>() == true);
  assert(b.isIndexed<char>() == true);
  assert(b.isIndexed<long>() == false);
  assert(b.isIndexed<size_t>() == false);
}
