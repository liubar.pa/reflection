# Reflection

Repository uses stateful metaprogramming via friend injection.

There are `ReflectionCounter` and `TypeIndexer` implemented. You can check `main.cpp` to understand how these classes work.

Take under consideration, those "tricks" is kind of dark magic, so dark g++ refuses to compile it, clang does compile, but it can crash. Locally, master branch compiles, but there is slightly modified Crashes branch, and, as it is name states, that branch crashes clang locally.
