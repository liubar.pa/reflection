#pragma once

#include "Loophole.hpp"
#include "ReflectionCounter.hpp"

#include <optional>

namespace detail {

template<typename UniqueType, typename T, typename ReflectionCounter>
struct TypeIndexerTag : loophole::ReflectionTag<UniqueType, T, ReflectionCounter>
{
};

template<typename UniqueType, typename T, typename ReflectionCounter>
struct TypeIndexerLoophole : loophole::ReflectionLoophole<UniqueType, T, ReflectionCounter>
{
  static constexpr auto Index = ReflectionCounter::increment();
};

};

// Don't redefine UniqueTag template argument
template<typename IndexType = size_t, IndexType StartIndex = 0, auto UniqueTag = []() {}>
class TypeIndexer
{
  using UniqueType = TypeIndexer<IndexType, StartIndex, UniqueTag>;

public:
  constexpr TypeIndexer()
  {
    loophole::checkUniqueness<UniqueType>();
  }

  TypeIndexer(const TypeIndexer& other) = delete;
  TypeIndexer& operator=(const TypeIndexer& other) = delete;

  TypeIndexer(TypeIndexer&& other) = default;
  TypeIndexer& operator=(TypeIndexer&& other) = default;

  // Don't redefine template argument OptimisationBlocker
  template<typename T, auto OptimisationBlocker = []() {}>
  static consteval IndexType addType()
  {
    if constexpr (!isIndexed<T>()) {
      setIndex<T>();
    }

    return detail::TypeIndexerLoophole<UniqueType, T, decltype(counter_)>::Index;
  }

  // Don't redefine template argument OptimisationBlocker
  template<typename T, auto OptimisationBlocker = []() {}>
  static consteval std::optional<IndexType> getIndex()
  {
    if constexpr (!isIndexed<T>()) {
      return std::nullopt;
    } else {
      return detail::TypeIndexerLoophole<UniqueType, T, decltype(counter_)>::Index;
    }
  }

  // Don't redefine template argument OptimisationBlocker
  template<typename T, auto OptimisationBlocker = []() {}>
  static consteval bool isIndexed()
  {
    return checkLoophole<T>();
  }

private:
  template<typename T, auto OptimisationBlocker = []() {}>
  static consteval bool checkLoophole()
  {
    return loophole::checkLoophole<detail::TypeIndexerTag<UniqueType, T, decltype(counter_)>>();
  }

  template<typename T, auto OptimisationBlocker = []() {}>
  static consteval void setLoophole()
  {
    loophole::setLoophole<detail::TypeIndexerLoophole<UniqueType, T, decltype(counter_)>>();
  }

  template<typename T, auto OptimisationBlocker = []() {}>
  static consteval void setIndex()
  {
    setLoophole<T>();
  }

  [[no_unique_address]] ReflectionCounter<IndexType, StartIndex, UniqueTag> counter_ ;
};
