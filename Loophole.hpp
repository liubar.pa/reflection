#pragma once

namespace loophole {

template<auto V>
struct ValueTag
{
  static constexpr auto Value = V;
};

template<typename UniqueType, typename... Info>
struct ReflectionTag
{
  friend consteval auto reflect(ReflectionTag<UniqueType, Info...>);
};

template<typename UniqueType, typename... Info>
struct ReflectionLoophole
{
  friend consteval auto reflect(ReflectionTag<UniqueType, Info...>)
  {
  }
};

// Don't redefine template argument OptimisationBlocker
template<typename ReflectionTag, auto OptimisationBlocker = []() {}>
consteval bool checkLoophole()
{
  return requires { reflect(ReflectionTag{}); };
}

// Don't redefine template argument OptimisationBlocker
template<typename ReflectionLoophole, auto OptimisationBlocker = []() {}>
consteval void setLoophole()
{
  (void) ReflectionLoophole{};
}

// Don't redefine template argument OptimisationBlocker
template<typename UniqueType, auto OptimisationBlocker = []() {}>
consteval void checkUniqueness()
{
  static_assert(
    !checkLoophole<ReflectionTag<UniqueType>>(),
    "UniqueType occurred second time, so it is not unique."
  );

  setLoophole<ReflectionLoophole<UniqueType>>();
}

};
